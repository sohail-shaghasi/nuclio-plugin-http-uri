<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\uri
{
	use nuclio\plugin\http\request\Request;
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	
	newtype URI_VECTOR	=ImmVector<string>;
	
	<<
		singleton,
		container
	>>
	class URI extends Plugin
	{
		const string URI_KEY		='REQUEST_URI';
		const string DEFAULT_URI	='/';
		const string DEFAULT_EMPTY	='';
		const string DEFAULT_START	='/';
		
		private string $URI;
		private URI_VECTOR $parts;
		
		public static function getInstance(string $URIStart=self::DEFAULT_START):URI
		{
			$instance=ClassManager::getClassInstance(self::class,$URIStart);
			return ($instance instanceof self)?$instance:new self($URIStart);
		}
		
		public static function getContainerInstance(string $container, string $URIStart=self::DEFAULT_START):URI
		{
			$instance=ClassManager::getContainerClassInstance($container,self::class,$URIStart);
			return ($instance instanceof self)?$instance:new self($URIStart);
		}
		
		public function __construct(string $URIStart=self::DEFAULT_START)
		{
			parent::__construct();
			$URI			=Request::getInstance()->getServer(self::URI_KEY);
			$this->URI		=is_string($URI)?$URI:self::DEFAULT_URI;
			if ($URIStart!==self::DEFAULT_URI)
			{
				$this->URI	=str_replace($URIStart,'',$this->URI);
			}
			list($this->URI)=explode('?',$this->URI,2);
			if ($this->URI==='')
			{
				$this->URI='/';
			}
			$parts			=new Vector(explode('/',$this->URI));
			if ($parts->get(0)==='')
			{
				$parts->removeKey(0);
			}
			if (!$parts->isEmpty() && $parts->get($parts->count()-1)==='')
			{
				$parts->pop();
			}
			if ($parts->isEmpty())
			{
				$parts[]='';
			}
			$lastIndex	=$parts->count()-1;
			$lastPart	=$parts->get($lastIndex);
			if (strstr($lastPart,'?'))
			{
				list($lastPart)=explode('?',$lastPart);
				$parts->set($lastIndex,$lastPart);
			}
			$this->parts=$parts->toImmVector();
		}
		
		public function getFull():string
		{
			return $this->URI;
		}
		
		public function getAllParts():URI_VECTOR
		{
			return $this->parts;
		}
		
		public function getPart(int $index):?string
		{
			if ($this->parts->containsKey($index))
			{
				return $this->parts->get($index);
			}
			return null;
		}
		
		public function isPartEmpty(int $index,bool $strict=false):bool
		{
			$part=$this->getPart($index);
			if (is_string($part))
			{
				if ($strict)
				{
					return (trim($part)===self::DEFAULT_EMPTY);
				}
				return ($part===self::DEFAULT_EMPTY);
			}
			return false;
		}
		
		public function getFirst():string
		{
			$first=$this->parts->get(0);
			return is_string($first)?$first:self::DEFAULT_EMPTY;
		}
		
		public function getLast():string
		{
			$count=$this->parts->count();
			$last=$this->parts->get($count-1);
			return is_string($last)?$last:self::DEFAULT_EMPTY;
		}
		
		public function getCount():int
		{
			return $this->parts->count();
		}
	}
}
